//
//  ViewController.swift
//  HelloSingleView
//
//  Created by Oskar Eek on 2019-10-22.
//  Copyright © 2019 Oskar Eek. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


    @IBAction func helloButton(_ sender: Any) {
        print("Hello button")
    }
}

